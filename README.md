# dataio_csv_read_write
Read/Write CSV files in C++

# Usage
## Loading data
    LoadDoubleData* data = new LoadDoubleData(<filename>, <delimiter>, <numberOfColumns>);

Access the data at data->dataArray[i] where i is the column number

## Saving data
    new SaveDoubleData(<filename>, <delimiter>, double** array, int numberOfColumns, int numberOfLines);

