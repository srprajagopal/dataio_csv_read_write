#include <iostream>
#include <fstream>
#include <ostream>
#include <iostream>
#include <cstdarg>

class SaveDoubleData{
  std::ofstream fileStream;
  
  int numberOfColumns;
  int numberOfLines;

 public:
  SaveDoubleData(std::string filename, char delimiter, int numberOfColumns, int numberOfLines, ...);
  void _saveInternal(std::string filename, char delimiter, double** arrayToWrite, int numberOfColumns, int numberOfLines);
  SaveDoubleData(std::string filename, char delimiter, double** arrayToWrite, int numberOfColumns, int numberOfLines);    
};

