#include <iostream>
#include <fstream>
#include <sstream>

class LoadDoubleData{
  std::ifstream fileStream;

  int numberOfColumns;
  int numberOfLines;
  
 public:
  double** dataArray;
  LoadDoubleData(std::string fileName, char delimiter, int numberOfColumns); 
  void printDataArray();
  std::size_t getNumberOfLines();
  std::size_t getNumberOfColumns();
};
