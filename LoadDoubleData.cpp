#include "LoadDoubleData.h"
#include <string.h>
#include <iostream>
#include <istream>

LoadDoubleData::LoadDoubleData(std::string fileName, char delimiter, int numberOfColumns){
  LoadDoubleData::numberOfColumns = numberOfColumns;
  
  // OPEN FILE
  fileStream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try{
    fileStream.open(fileName, std::ios::in);
  }
  // FAILURE opening file
  catch(std::ifstream::failure &e){
    std::cerr << "Exception opening file." << std::endl;
  }

  fileStream.exceptions(std::ifstream::badbit);
  // FILE OPEN successful. Start processing
  int lineNumber = 0;
  try{
    std::string line;
    dataArray = (double**) malloc(sizeof(double*) * numberOfColumns);

    // calculate number of lines in file
    // calculate line length
    std::getline(fileStream, line);
    int lineLength = line.size();

    // calculate file length
    fileStream.seekg(0, fileStream.end);    
    int fileLength = fileStream.tellg();

    // integer value of division gives number of lines approximately
    int numberOfLines = (fileLength/lineLength) * 2;
    LoadDoubleData::numberOfLines = numberOfLines;
    
    // assign memory to data array accordingly
    for(int i = 0 ; i < numberOfColumns ; i++){
      dataArray[i] = (double*) malloc(sizeof(double) * numberOfLines);
    }

    fileStream.seekg(0, fileStream.beg);    
    while(std::getline(fileStream, line)){
      lineNumber++;
      std::istringstream iss;
      iss.str(line);
      char** doubleValuesString = (char**) malloc(sizeof(char*) * numberOfColumns);

      // delimiter user supplied
      for(int i = 0 ; i < (numberOfColumns-1); i++){
        doubleValuesString[i] = (char*) malloc(sizeof(char) * (lineLength));
        iss.getline( *(doubleValuesString + i), 50, delimiter);
        *(*(dataArray + i) + lineNumber-1) = atof( *(doubleValuesString + i)   );
        
      }

      // delimiter as eol
      doubleValuesString[numberOfColumns - 1] = (char*) malloc(sizeof(char) * (lineLength));
      iss.getline( *(doubleValuesString + numberOfColumns - 1), 50);
      *(*(dataArray + numberOfColumns-1) + lineNumber-1) = atof( *(doubleValuesString + numberOfColumns-1));      
      for( int i = 0 ; i < (numberOfColumns - 1) ; i++){
        delete doubleValuesString[i];
      }
      delete[] doubleValuesString;
    }
  }
  // FAILURE reading file
  catch(std::ifstream::failure& e){
    std::cerr << "Exception while reading file." << std::endl << "Exception occured at line " << lineNumber << "." << std::endl;
    std::cerr << e.what() << std::endl;
  }

  // corrected number of lines
  LoadDoubleData::numberOfLines = lineNumber;
}

void LoadDoubleData::printDataArray(){
  for(int i = 0 ; i < numberOfLines ; i++){
    for(int j = 0 ; j < numberOfColumns ; j++){
      std::cout << *(*(dataArray + j) + i) << ":";
    }
    std::cout << std::endl;
  }
  
}

std::size_t LoadDoubleData::getNumberOfLines(){
  return numberOfLines;
}

std::size_t LoadDoubleData::getNumberOfColumns(){
  return numberOfColumns;
}
