#include "SaveDoubleData.h"

SaveDoubleData::SaveDoubleData(std::string filename, char delimiter, int numberOfLines, int numberOfColumns,  ...){
  double** arrayToWrite = (double**) malloc(sizeof(double*) * numberOfColumns);
  va_list args;
  va_start(args, numberOfColumns);
  for( int i = 0 ; i < numberOfColumns ; i++){
    arrayToWrite[i] = (double*) malloc(sizeof(double) * numberOfLines);
    arrayToWrite[i] = va_arg(args, double*);
  }
  _saveInternal(filename, delimiter, arrayToWrite, numberOfColumns, numberOfLines);  
}

void SaveDoubleData::_saveInternal(std::string filename, char delimiter, double** arrayToWrite, int numberOfColumns, int numberOfLines){
  SaveDoubleData::numberOfColumns = numberOfColumns;
  SaveDoubleData::numberOfLines = numberOfLines;

  // OPEN FILE FOR WRITING
  fileStream.exceptions(std::ofstream::failbit | std::ofstream::badbit);
  try{
    fileStream.open(filename, std::ios::out);
  }
  catch(std::ofstream::failure &e){
    std::cerr << "Exception opening file." << std::endl;    
  }

  fileStream.exceptions(std::ofstream::badbit);

  int j , i;
  try{
    for(i = 0 ; i < numberOfLines ; i++){
      for(j = 0 ; j < (numberOfColumns-1) ; j++){        
        fileStream << *( *(arrayToWrite + j) + i)  << delimiter;
      }
      fileStream << *( *(arrayToWrite + j) + i) << std::endl;
   } 
  }
  catch(std::ofstream::failure& e){
    std::cerr << "Exception while writing file." << std::endl << "Exception occured at " << j << "," << i << "." << std::endl;
    std::cerr << e.what() << std::endl;
  }
}

SaveDoubleData::SaveDoubleData(std::string filename, char delimiter, double** arrayToWrite, int numberOfColumns, int numberOfLines){
  _saveInternal(filename, delimiter, arrayToWrite, numberOfColumns, numberOfLines);
}
